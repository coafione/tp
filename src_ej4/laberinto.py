# -*- coding: utf-8 -*-
import sys

##### interfaz (metodos publicos)

class Laberinto(object):
    def __init__(self, parent=None):
        self.parent = parent
        self.laberinto = []
        self.rata = None
        self.queso = None
      
  
    def cargar(self, fn):
         self.laberinto=[]
         f = open(fn, "r")
         lab = []
         for lines in f:
             lab.append(lines)
         for i in range (1, len(lab)):
             a = self.procesa(lab[i])
             self.laberinto.append(a)
         self.queso = ((len(self.laberinto)-1), (len(self.laberinto[0])-1))
         self.rata = (0, 0)#self.setPosicionRata(0,0)
         return self.laberinto
	
#agregamos 0 si no fue visitado y no es parte del recorrido. 
#1 si fue visitado pero no es parte del camino actual. 2 visitado y parte del camino.
#empieza siendo todo 0 y se modifican en fn resolver

    def procesa(self,str):#toma cada string de lista y lo recorre de modo que devuelva los vectores
        if str[0] == '\n':
            return []
        else:
            a = list(str[1:8:2])
            a = [int(i) for i in a]
            a += [0]
            return [a] + self.procesa(str[9:])
	
    def tamano(self):
         if len(self.laberinto) == 0:
             return (0, 0)
         else:
             return (len(self.laberinto), len(self.laberinto[0]))

    def resetear(self):
        self.rata = (0,0)
        self.queso = ((len(self.laberinto)-1), (len(self.laberinto[0])-1))
        for i in range(len(self.laberinto)):
            for j in range(len(self.laberinto[0])):
                self.laberinto[i][j][4] = 0  #cambio todas las posiciones a NO VISITADAS
        return self.laberinto
			
#asumimos que no se puede conocer la posicion del queso ni de la rata si no hay un labrinto cargado

    def getPosicionRata(self):
        if self.rata == None:
             raise TypeError ('cargar un laberinto para conocer posicion')
        else:
             return self.rata
	
    def getPosicionQueso(self):
        if self.queso == None:
            raise TypeError ('cargar un laberinto para conocer posicion')
        else:
            return self.queso
	
    def setPosicionRata(self, i, j):#i es fila y j es columna
        if 0>i>=len(self.laberinto) or 0>j>=len(self.laberinto[0]):
            return False
        else:
            self.rata = (i, j)
            return True
	
    def setPosicionQueso(self, i ,j): 
        if 0>i>=len(self.laberinto) or 0>j>=len(self.laberinto[0]):
            return False
        else:
            self.queso = (i, j)
            return True
		
    def esPosicionRata(self,i,j):
         if self.rata == None:
             raise TypeError ('cargar un laberinto para conocer posicion')
         else:
             return self.rata == (i,j)
		
    def esPosicionQueso(self,i,j):
        if self.queso == None:
            raise TypeError('cargar laberinto para conocer posicion')
        else:
            return self.queso == (i,j)
 
#devulve True si hay pared. (1 en las listas)
           
    def get(self,i,j):
        if self.laberinto == []:
            raise TypeError ('El laberinto esta vacio')
        else:
            izq = self.laberinto[i][j][0]
            arriba = self.laberinto[i][j][1]
            derecha = self.laberinto[i][j][2]
            abajo = self.laberinto[i][j][3]
            return [izq==1, arriba==1, derecha==1, abajo==1]
	
#Si es 1 pude ser qu este visitda y no sea parte del camino, y si es 2 fue visitda y es parte del camino
 #para que sea parte del camino solo puede ser 2. 
    def getInfoCelda(self,i,j):
        diccionario = { "visitada": self.laberinto[i][j][4]!=0 , "caminoActual": self.laberinto[i][j][4]==2 }
        return diccionario
	

    def resuelto(self):
        return self.rata == self.queso


    def resolver(self):    
        i=self.getPosicionRata()[0]
        j=self.getPosicionRata()[1]   
        self.laberinto[i][j][4] = 2 #marco la posiciòn del labebrinto como parte del camino actual.
        if self.resuelto():
            return True
        else:
            if not self.get(i, j)[2] and j+1<len(self.laberinto[0]) and self.laberinto[i][j+1][4]==0: #derecha s/pared y con casillero no visitado ni parte del camino
                self.setPosicionRata(i, j+1)
                self._redibujar()
                if self.resolver():   #le tiene que preguntar a avanzar si llego al queso 
                    return True
                self.laberinto[i][j+1][4] = 1 #cambio la posicion del laberinto como VISITADO y NO parte del camino actual
                self.setPosicionRata(i, j)  #muevo la rata a la posicion anterior
               
            if not self.get(i, j)[3] and i+1<len(self.laberinto) and self.laberinto[i+1][j][4]==0:
                self.setPosicionRata(i+1, j)
                self._redibujar()
                if self.resolver():
                    return True   
                self.laberinto[i+1][j][4]=1
                self.setPosicionRata(i, j)
                
            if not self.get(i, j)[1] and i-1>0 and self.laberinto[i-1][j][4]==0:             
                self.setPosicionRata(i-1, j)            
                self._redibujar()
                if self.resolver():
                    return True   
                self.laberinto[i-1][j][4]=1
                self.setPosicionRata(i, j)
                
            if not self.get(i, j)[0] and j-1>0 and self.laberinto[i][j-1][4]==0:
                self.setPosicionRata(i, j-1)            
                self._redibujar()
                if self.resolver():
                    return True   
                self.laberinto[i][j-1][4]=1
                self.setPosicionRata(i, j)            
                
            else: #no hay camino al queso :(
                return False
            
        
    
    def _redibujar(self):
        if self.parent is not None:
            self.parent.update()

#
#
#programa
#input = sys.argv[1]
#l = Laberinto()
#l.cargar(input)
#print(l.resolver())