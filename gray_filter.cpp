#include "filters.h"
#include <omp.h>

void gray_filter(float * gray, pixel_t * img, int ii, int jj)
{
	#pragma omp parallel for collapse(2)  
	for(int i=0; i<ii; i++)
		{
        	for (int j=0; j<jj; j++)
        	    gray[i*jj + j] = 0.3*img[i*jj + j].r + 0.6*img[i*jj + j].g + 0.11*img[i*jj + j].b;
		}	
}	

