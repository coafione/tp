#declaracion de modulos
import sys
import time
import random
import matplotlib.pyplot as plt
import numpy as np
import math


input = sys.argv[1]

#declaracion de funciones

def listaDePuntos(fn):
	with open(fn, "r") as f:
		tupla = []
		for lines in f:
			lines = lines.split(" ")
			lines[0] = float(lines[0])
			lines[1] = float(lines[1])  # 2D
			lines = tuple(lines)
			tupla.append(lines)
		return(tupla)


def distancia(b,d):
    return (math.sqrt((b[0] - d[0])**2 + (b[1] - d[1])**2))
    
def distanciaMinima(a): #para aceder a cada par es a[i] para acceder a cda elemento del par es a[i][0]
    p1 = a[0]
    p2 = a[1] #este contador es para las menores distancia mientras recorro.
    for i in range (len(a)):
        for j in range (2, len(a)):
            if distancia(a[i], a[j]) < distancia(p1 , p2) and distancia(a[i], a[j]) != 0:
                p1 = a[i]
                p2 = a[j]
    return [p1,p2]
    
#PUNTO 3 ALGORITMOS de ORDENAMIENTO     ordenamos por las x.
#upsort
def maxPos(a,b,c):#devuelve la posicion del mayor
    res= b
    for j in range(b+1, c+1):
        if a[res][0] < a[j][0]:
            res = j
    return res
        
def upsort(a): #le entra una lista.
    posicion = len(a)-1
    m = 0
    while (posicion > 0):
        m = maxPos(a, 0, posicion)
        a[m] , a[posicion] = a[posicion] , a[m]
        posicion -= 1
    return a
        
#Ordenar mediante algoritmo merge: dividiendo recursivamente la lista inicial, cuando es suficientemente pequena se ordena con merge de a fragmentos (siempre ordenados por merges)  
def mergeSort(a): #le entra una lista y la devuelve ordeada. Utiliza recursion y una funcion auxiliar para ordenar mediante el algoritmo mergeSort.
    if len(a)<=1:
        return a
    else:
        k = len(a)//2
        return merges(mergeSort(a[:k]), mergeSort(a[k:]))
     
            
def merges(a,b):
    t = 0
    r = 0
    listaNueva = []
    while t<len(a) and r<len(b):
        if a[t][0] < b[r][0]:
            listaNueva.append(a[t])
            t += 1
        else:
            listaNueva.append(b[r])
            r +=1
    if t==len(a):
        listaNueva = listaNueva + b[r:]
    else:
        listaNueva = listaNueva + a[t:]
    return listaNueva 
            
            
 #ALGORITMOS para el calculo de las distancias
 
def distanciaMinimaDyC(a, algoritmo):#Segun el algorimo DyC:
    if algoritmo == "merge":
        a = mergeSort(a)
    elif algoritmo == "up":
        a = upsort(a)
    else:
        raise NameError('solo admite merge / up') 
    return disDyC(a)
    
def disDyC(a):
    #casos bases len(a) == 2 0 len(a) ==3
    if len(a) == 3:
        p1,p2 = distanciaMinima(a)
        return [p1,p2]
    elif len(a) == 2:
        dA = distancia(a[0],a[1])
        return [a[0],a[1]]
    else:
        mitad1 = disDyC(a[:len(a)//2]) #devuelven puntos por separado.
        mitad2 = disDyC(a[len(a)//2:])
 
#combine
        
        if distancia(mitad1[0],mitad1[1]) <= distancia(mitad2[0], mitad2[1]) and mitad1[0] != mitad1[1]:
            pt = [mitad1[0],mitad1[1]]
            dMIN = distancia(mitad1[0],mitad1[1])
        else: #si estos valen cero los tengo que filtrar en otro paso
            pt = [mitad2[0],mitad2[1]]
            dMIN = distancia(mitad2[0],mitad2[1])
#necesito la distancia horizontal de cada punto de la lista y fijarme si son menores a dMIN. 
  
        
        listaCENTRAL = []
        l = (len(a)//2)
        i = l-1
        while i >= 0 and dMIN >= abs(a[l][0] - a[i][0]): #quiero elegir si poner la cota o el l
                listaCENTRAL.append(a[i])
                i -= 1
    
        k = l
        while k < len(a) and dMIN >= abs(a[l][0] - a[k][0]):
                listaCENTRAL.append(a[k])
                k += 1 #maximo
        
        #listaCENTRAL.append(a[l])
        if len (listaCENTRAL) < 2:
            return pt
        elif len(listaCENTRAL) ==2:
            if distancia(listaCENTRAL[0],listaCENTRAL[1]) <= dMIN and distancia(listaCENTRAL[0],listaCENTRAL[1]) != 0:
                return listaCENTRAL
            else:
                return pt
              
        elif len(listaCENTRAL) > 2:
            p10,p20 = distanciaMinima(listaCENTRAL)
            dminCENTRAL = distancia(p10,p20)      
            if dminCENTRAL <= dMIN:
                return [p10,p20]
            else:
                return pt
    
                   
     
  


                
#GRAFICO
# genero lista con numeros aleatorios parahacer el grafico
def listaAleatoria(N):
    a = []
    for i in range(N):
        b = [0, 0]
        b[0] =round(random.random(),3)
        b[1] =round(random.random(),3)
        a.append(b)
    return a

# mido tiempos
def medirTiempos(funcion, b, c=''):
    tiempo = time.time()
    if c == '':
        funcion(b)
    else:
        funcion(b, c)
    return time.time()-tiempo 
    
def datos():
    tams = []
    t_fuerza_bruta = []
    t_mergesort = []
    t_upsort = []
    k_max = 10
    for i in range(10,501,30): #solo saco promedios para las listas chicas. 
        a = listaAleatoria(i)
        PromedioFuerzaBruta=0
        PromedioDyCupsort=0
        PromedioDyCmerge=0 
        
        for k in range(k_max): #saco promedio de tiempos cuando las listas son chicas.
            PromedioFuerzaBruta += medirTiempos(distanciaMinima, a)
            PromedioDyCupsort += medirTiempos(distanciaMinimaDyC, a, 'up')
            PromedioDyCmerge += medirTiempos(distanciaMinimaDyC, a, "merge")
        PromedioFuerzaBruta = PromedioFuerzaBruta / k_max
        PromedioDyCupsort = PromedioDyCupsort / k_max
        PromedioDyCmerge = PromedioDyCmerge / k_max
        tams.append(i)
        t_fuerza_bruta.append(PromedioFuerzaBruta)
        t_mergesort.append(PromedioDyCmerge)
        t_upsort.append(PromedioDyCupsort)
        
    for i in range(550,1500,50): #no saco promedios para tiempos grandes
        a = listaAleatoria(i) 
        TiempoFuerzabruta = medirTiempos(distanciaMinima, a)
        TiempoUpsort = medirTiempos(distanciaMinimaDyC, a, 'up')
        TiempoMergeSort = medirTiempos(distanciaMinimaDyC, a, "merge")
        tams.append(i)
        t_fuerza_bruta.append(TiempoFuerzabruta)
        t_mergesort.append(TiempoMergeSort)
        t_upsort.append(TiempoUpsort)
        print(i)

    plt.figure(1)
    plt.plot(tams, t_fuerza_bruta, label="Fuerza Bruta", color="red")
    plt.plot(tams, t_mergesort, label="MergeSort", color="blue")   
    plt.plot(tams, t_upsort, label="UpSort", color="orange")
    plt.legend()
    plt.title("Tiempos de ejecución")
    plt.xlabel("Tamaño de lista")
    plt.ylabel("Tiempo (s)")    
    plt.show()

# print(datos())



# programa principal 


z = listaDePuntos(input)
print(distanciaMinima(z))
print(distanciaMinimaDyC(z,"merge"))
print(distanciaMinimaDyC(z,"up"))
    
