class arbolBinario(object):
    def __init__(self):
        self.Raiz = None
        self.Der = None
        self.Izq = None
        
        
   #definimos un arbol vacío como aquel en el que su raiz es None     
    def vacio(self):
        return self.Raiz == None

    def raiz(self):
        if not self.vacio():
            return self.Raiz
        else:
            return "arbol vacio"
        
    def bin(self, a, izq, der):
        self.Raiz = a
        self.Der = der
        self.Izq = izq    
                        
    def izquierda(self):
       return self.Izq
    
    def derecha(self):
        return self.Der

#recorre el arbol empezando por la raiz, despues la rama izquierda y luego rama derecha.
#compara todos los elementos de entrada con el parametro a
        
    def find(self, a):  #funciona
        if a==self.Raiz:
            return True
        elif self.Der.vacio() and self.Izq.vacio():
            return False
        else:
            return self.Izq.find(a) or self.Der.find(a)

# devuelve otro arbol binario, espejo del que recibe.
            
    def espejo(self): 
        arbolEspejo = arbolBinario()
        if self.vacio():
            return arbolEspejo
        else:
            arbolEspejo.bin(self.Raiz, self.Der.espejo(), self.Izq.espejo())
            return arbolEspejo

#las tres funciones siguientes devuelven una lista vacia si reciben un arbol vacio. 

    def preorder(self):  
        if self.vacio():
            return []
        else:
            return [self.Raiz] + self.Izq.preorder() + self.Der.preorder()

    def posorder(self):
        if self.vacio():
            return []
        else: 
            return self.Der.posorder() + self.Izq.posorder() + [self.Raiz] 
    
    def inorder(self):
        if self.vacio():
            return []
        else:
           return self.Izq.inorder() + [self.Raiz] + self.Der.inorder() 
           
