import imfilters
from scipy import misc
import numpy as np
import time
import sys

input = sys.argv[1]

#devuelve una matriz correspondiente a una imagen en escala de grises a partir de una imagen en color.
def gray_filter(img):    
    filas = img.shape[0]
    columnas = img.shape[1]
    matrizRes = np.zeros((filas, columnas)) #genero una matriz de ceros de NumPy, del mismo tamaño que la matriz original.
    for i in range(filas):
        for j in range(columnas):        
            matrizRes[i,j] = int(0.3*img[i,j,0] + 0.6*img[i,j,1] + 0.11*img[i,j,2])
    return matrizRes

#devuelve una matriz correspondiente a una imagen difuminada, a partir de una imagen en escala de grises.
#ambas tienen el mismo tamaño

def blur_filter(img):
    filas = img.shape[0]
    columnas = img.shape[1]
    matrizRes = np.zeros((filas, columnas))  #la matriz res es una matriz de ceros. dejo los bordes como estan.
    for i in range(1, filas-1):
        for j in range(1, columnas-1):
            matrizRes[i,j] = ((img[i, j-1]) + (img[i, j+1]) + (img[i+1, j]) + (img[i-1, j])) /4
    return matrizRes



imagen_color = misc.imread(input)  

#mido el tiempo que tarda python en convertir la imagen en colores a escala de grises y guardarla. 
tiempoInicial = time.time()  
im_gray = gray_filter(imagen_color)
misc.imsave("grayfilter_py.jpeg", im_gray)
Tiempo_python = time.time() - tiempoInicial
print("Tiempo total de gray_filter en python:", Tiempo_python, "segundos")
misc.imsave("difuminado_py.jpeg", blur_filter(im_gray))


#mido el tiempo que se tarda en abrir la funcion en c y convertir la imagen en colores a escala de grises y guardarla. 
tiempoInicial = time.time()  
im_grayc = imfilters.gray_filter(imagen_color)
misc.imsave("grayfilter_c.jpeg", im_grayc)
Tiempo_c = time.time() - tiempoInicial
print("Tiempo total de gray_filter en c:", Tiempo_c, "segundos")
misc.imsave("difuminado_c.jpeg", imfilters.blur_filter(im_grayc))
