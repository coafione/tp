#include "filters.h"
#include <omp.h>

void blur_filter(float * im_res, float * im, int ii, int jj){
	#pragma omp parallel for collapse(2)
	for (int i=1; i<ii-1; i++) {
		for (int j=1; j<jj-1; j++) {	
			im_res[i*jj +j]= ((im[i*jj +j+1]) + (im[i*jj +j-1]) + (im[(i-1)*jj+j]) + (im[(i+1)*jj + j]) ) /4 ; 
				
		}
	}

}
