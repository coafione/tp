import unittest
import problem1


class TestArbolBinario(unittest.TestCase):  
    
       
    def test_vacio(self):
        x = problem1.arbolBinario()
        self.assertTrue(x.vacio()) #prueba que la función vacío funcione
        c = problem1.arbolBinario()
        t = problem1.arbolBinario()
        c.bin(2, problem1.arbolBinario(), problem1.arbolBinario())
        t.bin(8, problem1.arbolBinario(), problem1.arbolBinario())
        x.bin(1,t,c)
        self.assertFalse(x.vacio()) #falla si arbol esta vacío(si bin no funciona)
        
        
# verifica que la funcion preorder devuelva una lista en la que el primer elemento sea la raiz, despues la rama izquiera y despues la derecha. 
# Para eso se crea un arbol tal que al aplicarle preorder (si funcionara) devuelva una lista creciente.    
# además verifica que la función devuelva una lista vacía si le entra un arbol vacío.
        
    def test_preorder (self):
        a = problem1.arbolBinario()
        b = problem1.arbolBinario()
        c = problem1.arbolBinario()
        d = problem1.arbolBinario()
        e = problem1.arbolBinario()
        p = problem1.arbolBinario()        
        e.bin(5, problem1.arbolBinario(), problem1.arbolBinario())
        d.bin(4, problem1.arbolBinario(), problem1.arbolBinario())
        c.bin(3, d, e)
        b.bin(2, problem1.arbolBinario(), problem1.arbolBinario())
        a.bin(1, b, c)
        lista_preorder = a.preorder()
        for i in range(len(lista_preorder)-1):
            self.assertLess(lista_preorder[i], lista_preorder[i+1])
        k = len(p.preorder())  #p es un arbol vacío. el largo de la lista deberia ser 0 si la función anda.
        self.assertTrue(k==0)          
            
           
#Se le pasa un valor de un elemento que esta en el arbol a la función find.

    def test_find(self):
        a = problem1.arbolBinario()
        b = problem1.arbolBinario()
        c = problem1.arbolBinario()
        d = problem1.arbolBinario()
        e = problem1.arbolBinario()
        e.bin(5, problem1.arbolBinario(), problem1.arbolBinario())
        d.bin(4, problem1.arbolBinario(), problem1.arbolBinario())
        c.bin(3, d, e)
        b.bin(2, problem1.arbolBinario(), problem1.arbolBinario())
        a.bin(1, b, c)
        self.assertTrue(a.find(4)) #solo falla si no encuentra un elemento que si esta en el arbol.
        self.assertFalse(a.find(9))  #solo falla si "encuentra" el elemento (que no esta en el arbol) en el arbol 
    
if __name__ == "__main__":
    unittest.main()
          
       
